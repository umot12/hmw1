package com.danit.springrest.model.service;

import com.danit.springrest.model.Customer;
import com.danit.springrest.model.dao.CustomerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;
@Service
public class DefaultCustomerService implements CustomerService {
    private CustomerDao mockcustomerDao;
@Autowired
    public DefaultCustomerService(CustomerDao mockcustomerDao) {
        this.mockcustomerDao = mockcustomerDao;
    }


    public Customer save(Customer customer) {


        return mockcustomerDao.save(customer);
    }

    public boolean delete(Customer customer) {
        return mockcustomerDao.delete(customer);
    }


    public void deleteAll(List<Customer> customers) {
        mockcustomerDao.deleteAll(customers);
    }

    public List<Customer> findAll() {
        return mockcustomerDao.findAll();
    }

    public void saveAll(List<Customer> customers) {
        mockcustomerDao.saveAll(customers);
    }

    public boolean deleteById(long id) {
        return mockcustomerDao.deleteById(id);

    }

    public Customer getOne(long id) {

        return mockcustomerDao.getOne(id);


    }
}