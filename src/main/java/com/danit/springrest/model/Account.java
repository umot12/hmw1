package com.danit.springrest.model;


import lombok.EqualsAndHashCode;

import java.util.Objects;
import java.util.UUID;
@EqualsAndHashCode
public class Account {


private  Long id ;
 private String number =UUID.randomUUID().toString();

private  Currency currency;

 private Double balance = (double) 0;


 private Customer customer;




    public Account(Currency currency, Customer customer) {
        this.currency = currency;
        this.customer = customer;
    }


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", currency=" + currency +
                ", balance=" + balance +
                ", customer=" + customer +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account account)) return false;
        return Objects.equals(getId(), account.getId()) && Objects.equals(getNumber(), account.getNumber()) && getCurrency() == account.getCurrency();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNumber(), getCurrency());
    }
}
