package com.danit.springrest.model;

public enum  Currency {
    USD,
    EUR,
    UAH,
    CHF,
    GBP
}
