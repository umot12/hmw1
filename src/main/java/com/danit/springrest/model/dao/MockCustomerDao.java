package com.danit.springrest.model.dao;

import com.danit.springrest.model.Account;
import com.danit.springrest.model.Currency;
import com.danit.springrest.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Repository
@Lazy
public class MockCustomerDao implements CustomerDao {
    private List<Customer> customers = new ArrayList<>();
@Autowired
    public MockCustomerDao(List<Customer> customers ) {
        this.customers  = customers ;
    }





    public  Customer   save(Customer  customer ){
        customer .setId(Long.parseLong(String.valueOf(customers .size()  + 1)));
        customers .add(customer );
      return customer ;
    }

    public   boolean delete(Customer  customer ){
        customers .remove(customer) ;
        if(customers .contains(customer )){
            return false;
        }
        return true ;
    }


    public   void deleteAll(List <Customer > customers ){
        customers .clear();
    }

    public   List<Customer  > findAll(){
        return customers  ;
    }
    public   void saveAll (List <Customer  > customers ) {
        this.customers  = customers ;
    }

    public    boolean deleteById(long id){
        Optional  <Customer> customerOptional = customers .stream().filter(el-> el.getId().equals(id)).findAny();
        if(customerOptional.isEmpty()){
            return false;
        }else{
            customers .remove(customerOptional.get());
            return true;

        }

    }
    public   Customer  getOne(long id){

        Optional  <Customer> customerOptional = customers.stream().filter(el-> el.getId().equals(id)).findAny();
        if(customerOptional.isEmpty()){
            return null;
        }else{

            return  customerOptional.get();

        }


    }



}
